# Final Project Laravel

## Kelompok 5

## Anggota Kelompok

-   Rani Kusuma Afriani
-   Yohana Stefiana
-   M.Raehan Gustia
-   M.Pierre Richardson

## Tema Project

Forum Tanya Jawab

## ERD

![ERD_Final_Project_Fix.png](ERD_Final_Project_Fix.png?raw=true)

## Link Video dan Deploy

Link Deploy : https://titip-2.rismajt.or.id/

Link Demo Aplikasi : https://drive.google.com/file/d/1rYIEwipRpU-Bz03_RE2oCtdlWePnqzAB/view?usp=sharing

Link Template :https://themewagon.com/themes/free-responsive-bootstrap-5-html5-admin-template-sneat/
